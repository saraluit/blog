<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 200);
            $table->string('introducao');
            $table->text('descricao');
            $table->tinyInteger('status')->default(0);
            $table->string('imagem', 200);
            $table->timestamps();

            $table->unsignedBigInteger('user_id');
            $table->foreign('use_id')->references('id')->on('users');


            $table->foreign('categoria_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria');
    }
};
